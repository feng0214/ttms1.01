###解决Mybatis在IDEA中找不到mapper映射文件
刚开始在IDEA中做用Mybatis查数据库中的数据时，报mapper映射文件找不到，配置的路径正确，没有拼写错误。
（在eclipse中这样写是没问题的）
原因是IDEA不会编译src的java目录下的xml文件。
解决方法就是在pom.xml文件中加上配置，让编译器把src/main/java目录下的xml文件一同编译到classes文件夹下。
    <build>
        <resources>
            <resource>
                <directory>src/main/java</directory>
                <includes>
                    <include>**/*.xml</include>
                </includes>
            </resource>
        </resources>
    </build>
    