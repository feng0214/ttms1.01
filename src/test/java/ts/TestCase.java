package ts;

import cn.tedu.ttms.common.web.PageObject;
import cn.tedu.ttms.product.dao.ProjectDao;
import cn.tedu.ttms.product.entity.Project;
import cn.tedu.ttms.product.service.ProjectService;
import cn.tedu.ttms.product.service.impl.ProjectServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;
import java.util.Map;

public class TestCase {
    ClassPathXmlApplicationContext ac;
    @Autowired
    private ProjectService projectService;
    @Before
    public void init(){
        ac = new ClassPathXmlApplicationContext("spring-mvc.xml","spring-mybatis.xml");
    }
    @Test
    public void test(){
        projectService = ac.getBean("projectServiceImpl", ProjectServiceImpl.class);
        List<Project> list = projectService.findObjects();
        Assert.assertNotEquals(0,list.size());
        System.out.println(list.size());
    }
    @Test
    public void test1(){
        projectService = ac.getBean("projectServiceImpl", ProjectServiceImpl.class);
        Map<String,Object> map = projectService.findPageObjects("环球",1,1);
        List<Project> list=(List<Project>)map.get("list");
        PageObject pageObject = (PageObject) map.get("pageObject");
        Assert.assertEquals(1,list.size());
        Assert.assertEquals(1,pageObject.getPageCount());
        System.out.println(list);
    }
    @Test
    public void test2(){
        projectService = ac.getBean("projectServiceImpl", ProjectServiceImpl.class);
        String ids = "4,3";
        Integer valid=1;
        projectService.validById(valid,ids);
    }
    @After
    public void destroy(){
        ac.close();
    }
}
