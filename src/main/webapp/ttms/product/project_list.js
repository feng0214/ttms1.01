$(function () {
	$("#queryFormId").on("click",".btn-search",doQueryObjects)
		.on("click",".btn-valid,.btn-invalid",doValidById)
	doGetObjects();
})
function doValidById(){
	var valid;
	if($(this).hasClass("btn-valid")){
		valid = 1;//启用
	}
	if($(this).hasClass("btn-invalid")){
		valid = 0;//禁用
	}
	//获得选中的checkbox的id
	var ids="";
	$("#tbodyId input[name='checkId']").each(function () {
		if ($(this).prop("checked")){
			if (ids==""){
				ids+=$(this).val();
			}else{
				ids+=","+$(this).val();
			}
		}
	});
	console.log(ids)
	console.log(valid)
	if (ids==""){
		alert("请至少选择一个");
		return;
	}
	//发起异步请求，更新数据
	var url = "project/doValidById.do"
	var params = {
		"valid":valid,
		"ids":ids
	}
	$.post(url,params,function(){
		doGetObjects();
	})
}
function doQueryObjects(){
	$("#pageId").data("pageCurrent",1);
	doGetObjects();
}
/*获取项目信息*/
function doGetObjects(){
	var url = "project/doGetPageObjects.do";
	var pageCurrent=$("#pageId").data("pageCurrent");
	if(!pageCurrent)pageCurrent=1;
	var params={"pageCurrent":pageCurrent};
	params.name=$("#searchNameId").val();
	params.valid=$("#searchValidId").val();
	$.getJSON(url,params,function (result) {
		console.log(result);
		//将数据添加在
		setTableBodyRows(result.list);
		setPagination(result.pageObject);
	})
}
function setTableBodyRows(result) {
	var tbady=$('#tbodyId');
	tbady.empty();
	for (var i in result){
		console.log(result[i]);
		var tr=$("<tr></tr>");
		var tds = "<td><input type='checkbox' name='checkId' value='"+result[i].id+"'></td>"+
			"<td>"+result[i].code+"</td>"+
			"<td>"+result[i].name+"</td>"+
			"<td>"+result[i].beginDate+"</td>"+
			"<td>"+result[i].endDate+"</td>"+
			"<td>"+(result[i].valid?'启用':'禁用')+"</td>"+
			"<td><input type='button' value='修改' class='btn btn-default'></td>";
		tr.append(tds);
		tbady.append(tr);
	}

	//获得tbody对象
	//迭代数据集
	//构建突然对象
	//构建每行td对象
}
