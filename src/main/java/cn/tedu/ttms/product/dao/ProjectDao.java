package cn.tedu.ttms.product.dao;

import cn.tedu.ttms.product.entity.Project;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**项目模块持久层对象*/
public interface ProjectDao {
    /**查询所有项目信息*/
    List<Project> findObjects();
    List<Project> findPageObjects(@Param("name")String name,@Param("valid")Integer valid,@Param("startIndex")int startIndex, @Param("pageSize") int pageSize);
    int getRowCount(@Param("name") String name,@Param("valid")Integer valid);
    int validById(@Param("valid") Integer valid,@Param("ids")String[] ids);
}
