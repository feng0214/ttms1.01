package cn.tedu.ttms.product.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Administrator
 *
 */
public class Project implements Serializable {

    private static final long serialVersionUID = -8617376423425257328L;

    private Integer id;
    /**项目编码*/
    private String code;
    /**项目名称*/
    private String name;
    /**项目开始时间*/
    @JsonFormat(pattern = "yyyy/MM/dd")
    private Date beginDate;
    /**项目结束时间*/
    @JsonFormat(pattern = "yyyy/MM/dd")
    private Date endDate;
    /**项目状态*/
    private Integer valid;
    /**项目备注*/
    private String node;
    /**创建用户*/
    private String createdUser;
    /**修改用户*/
    private String modifiedUser;
    /**创建时间*/
    @JsonFormat(pattern = "yyyy/MM/dd")
    private Date createdTime;
    /**修改时间*/
    @JsonFormat(pattern = "yyyy/MM/dd")
    private Date modifiedTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getValid() {
        return valid;
    }

    public void setValid(Integer valid) {
        this.valid = valid;
    }

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", beginDate=" + beginDate +
                ", endDate=" + endDate +
                ", valid=" + valid +
                ", node='" + node + '\'' +
                ", createdUser='" + createdUser + '\'' +
                ", modifiedUser='" + modifiedUser + '\'' +
                ", createdTime=" + createdTime +
                ", modifiedTime=" + modifiedTime +
                '}';
    }
}
