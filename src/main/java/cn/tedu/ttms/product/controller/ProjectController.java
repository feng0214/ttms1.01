package cn.tedu.ttms.product.controller;

import cn.tedu.ttms.common.web.JsonResult;
import cn.tedu.ttms.product.entity.Project;
import cn.tedu.ttms.product.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Controller
@RequestMapping("/project/")
public class ProjectController {
   // private static Logger logger = Logger.getLogger(ProjectController.class.getSimpleName());
    @Autowired
    private ProjectService projectService;
    /**返回项目列表页面*/
    @RequestMapping("listUI")
    public String indexUI(){
        return "product/project_list";
    }
    @RequestMapping("doGetObjects")
    @ResponseBody
    public List<Project> doGetObjects(){
        List<Project> list = projectService.findObjects();
       // logger.info("list:"+list);
        return list;
    }
    @RequestMapping("doGetPageObjects")
    @ResponseBody
    public Map<String,Object> doGetPageObjects(String name,Integer valid,Integer pageCurrent){
       Map<String,Object> map = projectService.findPageObjects(name,valid,pageCurrent);
        return map;
    }

    @RequestMapping("doValidById")
    @ResponseBody
    public void doValidById(Integer valid, String ids){
        projectService.validById(valid,ids);

    }
}
