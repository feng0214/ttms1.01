package cn.tedu.ttms.product.service;

import cn.tedu.ttms.product.entity.Project;

import java.util.List;
import java.util.Map;

public interface ProjectService {
    List<Project> findObjects();
    Map<String,Object> findPageObjects(String name,Integer valid,int pageCurrent);
    void validById(Integer valid,String ids);
}
