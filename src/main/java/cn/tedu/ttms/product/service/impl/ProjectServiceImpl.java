package cn.tedu.ttms.product.service.impl;

import cn.tedu.ttms.common.exception.ServiceException;
import cn.tedu.ttms.common.web.PageObject;
import cn.tedu.ttms.product.dao.ProjectDao;
import cn.tedu.ttms.product.entity.Project;
import cn.tedu.ttms.product.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProjectServiceImpl implements ProjectService {
    @Autowired
    private ProjectDao projectDao;
    @Override
    public List<Project> findObjects() {
        List<Project> list = projectDao.findObjects();
        return list;
    }

    @Override
    public Map<String, Object> findPageObjects(String name,Integer valid,int pageCurrent) {
        int pageSize=2;
        int startIndex = (pageCurrent-1)*pageSize;
        List<Project> list = projectDao.findPageObjects(name,valid,startIndex,pageSize);
        int rowCount = projectDao.getRowCount(name,valid);
        PageObject pageObject = new PageObject();
        pageObject.setRowCount(rowCount);
        pageObject.setPageSize(pageSize);
        pageObject.setPageCurrent(pageCurrent);
        pageObject.setStartIndex(startIndex);
        Map<String,Object> map=new HashMap<>();
        map.put("list",list);
        map.put("pageObject",pageObject);
        return map;
    }

    @Override
    public void validById(Integer valid, String ids) {
        System.out.println(valid==1);
        if(valid != 0 && valid != 1) {
            throw new ServiceException("valid的值不合法，valid="+valid);
        }
        if(StringUtils.isEmpty(ids)) {
            throw new ServiceException("ids的值不能为空");
        }
        String[] idArray = ids.split(",");
        int rows = projectDao.validById(valid,idArray);
        if(rows==0) {
            throw new ServiceException("修改失败");
        }

    }


}
